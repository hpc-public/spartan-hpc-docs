## SSH Agent Forwarding

Whilst Spartan requires use of Secure Shell (SSH) in preference to remote login systems that send data in plain-text (e.g., rlogin, telnet, ftp), Spartan administrators also _strongly_ recommend against the storing of SSH private keys on the system, and prohibit the storing of unencrypted private keys. [Supercomputers have been targetted](https://www.zdnet.com/article/supercomputers-hacked-across-europe-to-mine-cryptocurrency/) to mine cryptocurrency by exploiting compromised SSH credentials. 

It is not _necessary_ to have a private key on Spartan, which is a shared system. When one puts their private key on a shared system or server it means whoever has access to that system may have access to the private key and therefore may be able to impersonate that user.

Instead of putting on SSH key on a system like Spartan, use the `ssh -A` command, which is "SSH agent forwarding". It is necessary to add the key to the local agent. 

e.g., `ssh-add ~/.ssh/id_rsa`

Then log in with agent forwarding.

```
$ ssh -A username@spartan.hpc.unimelb.edu.au
[lev@spartan-login1 ~]$ ssh othersystem@example.com
```

Using this system the *private* SSH key remains in one private location.

## Encrypt Private Keys

When creating a private key, SSH recommends entering a passphrase. Although it is an option, it is *not* recommended that is left blank. The term "passphrase" is deliberate. Rather than a short password, consider a longer phrase or sentence which will have [a higher level of entropy](https://xkcd.com/936/), and thus be more difficult to "brute force" a result.

With existing private keys it is possible to encrypt them with the following command:

```
ssh-keygen -o -p -f keyfile
```

## SSH Config Files

An SSH config file allows one to create alises (i.e. shortcuts) for a given hostname. 

Create the text file in `~/.ssh` directory and enter the following:

```
Host *
ServerAliveInterval 120
Host spartan
       Hostname spartan.hpc.unimelb.edu.au
       User username
```

where username is your Spartan username. 

Now to connect to Spartan, one only needs to type `ssh spartan`.

## Passwordless SSH

Passwordless SSH for Spartan is easier for users, automation of scripts, and is necessary for some applications.

### For Linux and MacOS

To set this up for *nix system (e.g., UNIX, Linux, MacOS X) start with terminal on *local* system and generate a keypair.

```
$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/user/.ssh/id_rsa): 
Created directory '/home/user/.ssh'.
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/user/.ssh/id_rsa.
Your public key has been saved in /home/user/.ssh/id_rsa.pub.
The key fingerprint is:
43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8 user@localhost
```

Append the new public key to `~/.ssh/authorized_keys` on Spartan (and enter the password, for the last time).

```
$ cat .ssh/id_rsa.pub | ssh username@spartan.hpc.unimelb.edu.au 'mkdir ~/.ssh; cat >> ~/.ssh/authorized_keys; chmod 700 ~/.ssh; chmod 600 ~/.ssh/authorized_keys'
```

### MS-Windows

This is somewhat more difficult, requiring several steps: 

1) Download additional software called PuTTYgen
`https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html`

2) Launch up PuTTYgen tool up. If you are on Windows 7 higher, right-click on it and select Run as Administrator.

3) Select the parameters; the default value (SSH-2 RSA) is fine.

4) Select Generate

5) Add the public key to the authorized_keys file in ~/.ssh on Spartan (create it if it doesn't exist). Ensure there are no unexpected line-breaks. Make sure the permissions on the file are 0600 and .ssh directory are 700.

`chmod 600 ~/.ssh/authorized_keys; chmod 700 ~/.ssh`

6) Back on PuTTYgen save the Private Key and Public Key. Make sure to save Public Key as .txt while Private Key as .ppk.

7) Configure Putty to use that newly generated key. Start putty and go to Connection > SSH > Auth and add the locate of the Private Key saved previously.

8) Open Putty and login as usual. If all the steps above have been followed no password will be required.

Screenshots and a Youtube video on how to do this can be found on [https://www.ssh.com/ssh/putty/windows/puttygen](https://www.ssh.com/ssh/putty/windows/puttygen).

