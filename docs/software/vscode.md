Microsoft VSCode is a common tool for software development, and can be used with Spartan.

Please note however that a few changes must be made to your settings.json file to ensure that the login nodes do not become overloaded. This is because the filewatcher and file searcher (rg) index all the files you have access to in your workspace.

If you have a large dataset (e.g. machine learning) this can take a lot of resources on the login node.

!!! Important
      The changes must be synced to "~/.vscode-server/data/Machine/settings.json" file on Spartan and not just in your desktop/laptop's settings.json. Choose the appropriate Remote SSH sync setting option(s) in VSCode to synchronize your exclude settings to the remote host (Spartan login node).



!!! example
    === "settings.json"
        ```
        "files.watcherExclude": {
          "**/.git/objects/**": true,
          "**/.git/subtree-cache/**": true,     
          "**/node_modules/*/**": true,
          "/apps/**": true,
          "/data/gpfs/**": true,
          "/data/projects/**": true,
          "/data/cephfs/**": true,
          "/data/scratch/**": true,
          "/scratch/**": true},

        "search.followSymlinks": false,

        "search.exclude": {     
          "**/.git/objects/**": true,
          "**/.git/subtree-cache/**": true,     
          "**/node_modules/*/**": true,
          "/apps/**": true,
          "/data/gpfs/**": true,
          "/data/projects/**": true,
          "/data/cephfs/**": true,
          "/data/scratch/**": true,
          "/scratch/**": true},
        ```
