## Environment Modules

In many computer environments only one version of an application is installed. On a shared system for researchers, this would not be a good idea. We need to have different versions of applications so that (a) users can be consistent in what version of a software application they are using, and relay that to others to help reproducibility and (b) users can access different features and developments which are introduced in different versions of a software application.

Spartan uses an environment modules system ([lmod](http://lmod.readthedocs.io/en/latest/)) to load and unload different applications to a user's $PATH, the variable which tracks what software is available to a user's environment. The environment modules system allows many different software applications to be installed on Spartan at once without interfering with each other.

- To see what software applications and versions that a user current has in their $PATH the command `module list` can be used.

- To check global wide applications and a summary description of each application using the `module spider` command 

- To check what's currently installed applications in particular toolchain use `module list` to make sure Toolchain is loaded, if not `module load <Toolchain>`, then `module avail`.

- To load a particular application use the `module load` command. For example, to load GCC/11.3.0, use `module load GCC/11.3.0`. 

- To unload a module's environment from a user's $PATH use the `module unload` command, for example, `module unload GCC/11.3.0`. 

- To unload one module and load another use the `module switch` command, for example, `module switch Python/3.10.4 Python/3.10.4-bare`.

Often modules load other modules. A very common example is `module load foss/2022a`, which loads `GCC/11.3.0` and `OpenMPI/4.1.4` and their respective chains of software.

Generally you shouldn't need to load modules from the login node, instead working on a compute node, either via an interactive session (launched with `sinteractive`), or from within your Slurm script.

## Toolchains

A toolchain is a combination of compiler and dependent libraries that are then used to build software.  

On spartan we have setup default toolchain to foss/2022a which include GCCcore/11.3.0, GCC/11.3.0, gompi/2022a 

We have a few types of toolchains on Spartan.  

    GCCcore - just the GCC compiler suite  

    GCC - GCCcore and binutils  

    gompi - GCC and OpenMPI  

    foss - gompi and FlexiBLAS, FFTW and ScaLAPACK

    intel-compilers - Intel C, C++ & Fortran compilers

    iompi - Intel C/C++ and Fortran compilers, alongside Open MPI

    iomkl - Intel compilers, Open MPI and Intel Math Kernel Library (MKL)

    NVHPC - C, C++ and Fortran compilers included with the NVIDIA HPC SDK (previously: PGI)

 
To list available toolchains
```
$ module_list_toolchains
```
For more information, please see [Easybuild](https://docs.easybuild.io/common-toolchains/#toolchains_diagram)  

## The New Modules System

The Spartan HPC has developed a new build system which includes the latest versions of software applications with newer toolchains. A feature of the build system is that it explicitly protects users from mixing toolchains in the same job submission script. 

When using the modules system a view of modules will explicitly list only the modules that you have available for the toolchain in question. A user must load the appropriate toolchain to ensure that the right applications are available. If one tries to load a module, without loading a toolchain first, an error will be generated. Note that modules are case-sensitive.

```
$ module load Python/3.10.4
Lmod has detected the following error:  These module(s) or extension(s) exist but cannot be loaded as requested:
"Python/3.10.4"
   Try: "module spider Python/3.10.4" to see how to load the module(s)..
```

Instead, load a toolchain first.

`$ module load GCCcore/11.3.0; module load Python/3.10.4`

## Common scenarios and which modules to load

Below are common work scenarios and which toolchain and modules to load:

!!! example

    === "Scientific Python applications"

        ```
        module load foss/2022a Python/3.10.4 SciPy-bundle/2022.05
        ```
        
        This will load Python 3.10 with access to numpy, scipy, mpi4py and pandas

    === "R scripts"

        ```
        module load foss/2022a R/4.2.1
        ```

        This will load R 4.2.1, as well as Python 3.10, Fribidi and Harfbuzz (needed for a lot of R packages)

    === "Tensorflow with GPU"

        ```
        module load foss/2022a Python/3.10.4 TensorFlow/2.11.0-CUDA-11.7.0
        ```

        This will load Python 3.10 with Tensorflow 2.11, CUDA 11.7.0 and cudNN 8.4

    === "GPU enabled application"

        ```
        module load foss/2022a CUDA/11.7.0 UCX-CUDA/1.13.1-CUDA-11.7.0 cuDNN/8.4.1.50-CUDA-11.7.0
        ```

        This will load CUDA, cudNN and associated packages to accelerate inter-gpu communication

    === "Gaussian"

        ```
        module purge
        module load NVHPC/22.11-CUDA-11.7.0 Gaussian/g16c01-CUDA-11.7.0
        ```

        This will load the NVHPC toolchain, and Gaussian 16


## Software List and New Software

Every day a list of [software installed](/softwarelist.txt) on Spartan is regenerated from `module spider` and added to this page. This does not list all versions on the summary line. Other software can be installed on request.

If the software list doesn't include what one is after [Get in contact](/training_help/) with us and we can install it for you. Generally speaking, you should avoid compiling software on Spartan, unless you wrote it from scratch for your own use. By letting us handle it, we can make sure that:

* It works
* Software licenses are managed
* Code is compiled with the appropriate flags to maximize performance
* Other users can also make use of the software.

## SBGrid

To allow faster access to new versions for structural biology users, we have made [SBGrid](https://sbgrid.org/) available on Spartan. To use it, do the following in your terminal and/or Slurm scripts:  

```
source /programs/sbgrid.shrc
```

After you source the sbgrid.shrc file, you will be able to use all the applications in the [SBGrid Documentation](https://sbgrid.org/wiki/examples/getting_started)  
