The Gurobi Optimizer is a state-of-the-art solver for mathematical programming.

We have Gurobi installed on Spartan, and have an academic license available for Spartan users.

!!! example
    === "Load Gurobi module"
        ```
        module load Gurobi/10.0.1-Python-3.10.4
        ```

    === "Setting license"
        If you use our Gurobi module, the license environment variable is automatically set

        If not, you will have to set an environment variable for the license

        ```
        export GRB_LICENSE_FILE=/apps/easybuild-2022/easybuild/software/Compiler/GCCcore/11.3.0/Gurobi/10.0.1-Python-3.10.4/gurobi.lic
        ```
