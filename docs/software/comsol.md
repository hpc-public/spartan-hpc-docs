## GUI

COMSOL is able to run in GUI mode via Open OnDemand by using the following command in the terminal application in Virtual Desktop.

```
module load COMSOL/6.2
comsol -3drend sw
```

For how to run Open OnDemand, Please see [How to use Open OnDemand](/webenvironments/openondemand/).

## License

We do not have a central license for COMSOL to make it available to everyone. You will need to obtain the license information from the group
who purchased the COMSOL license. You will need the port and the hostname of the license server. Once you have that, before starting COMSOL, do

```
export LM_LICENSE_FILE=port@hostname
```

either in your terminal or batch submit scripts
