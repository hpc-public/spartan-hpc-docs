## License

We do not have license for Abaqus. You will need to contact your department for license information from the group
who purchased the Abaqus license. You will need the port and the hostname of the license server. Once you have that, before starting Abaqus, do

```
export LM_LICENSE_FILE=port@hostname
```

either in your terminal or batch submit scripts


## Abaque script

We need to add 'interactive' at the end of abaqus line to make the program run smoothly.

```
abaqus job=JOB_NAME cpus=CORES mp_mode=threads interactive
```

