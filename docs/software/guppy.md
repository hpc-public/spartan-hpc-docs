Guppy is a data processing toolkit that contains the Oxford Nanopore Technologies’ basecalling algorithms, and several bioinformatic post-processing features. It is provided as binaries to run on Windows, OS X and Linux platforms, as well as being integrated with MinKNOW, the Oxford Nanopore device control software.

Guppy provides `guppy_aligner`, `guppy_basecaller` and `guppy_barcoder`, and can run on GPUs and CPUs.

Due to the software license, we cannot install Guppy in our central software location, but it is simple to download, expand and then use in your jobs.

!!! example
    === "Download Guppy"
        Guppy is distributed as a tar archive, and is available to ONT customers via their community website [https://community.nanoporetech.com/](https://community.nanoporetech.com/).

        The file normally has a filename like `ont-guppy_version_linux64.tar.gz` (GPU version) and `ont-guppy-cpu_version_linux64.tar.gz` (CPU version).

        Download the relevant file and transfer it to your project directory on Spartan.

    === "Expand"
        The Guppy tar archive can be expanded using `tar`. In the following example, we have transferred the guppy archive to `/data/gpfs/projects/punim0001/guppy`

        ```
        [scrosby@spartan-login1 ~]$ cd /data/gpfs/projects/punim0001/guppy
        [scrosby@spartan-login1 guppy]$ ls
        ont-guppy_6.1.2_linux64.tar.gz
        [scrosby@spartan-login1 guppy]$ tar -zxf ont-guppy_6.1.2_linux64.tar.gz 
        [scrosby@spartan-login1 guppy]$ ls
        ont-guppy  ont-guppy_6.1.2_linux64.tar.gz
        ```

        This has expanded ont-guppy_6.1.2_linux64.tar.gz into the directory `ont-guppy`

    === "Use Guppy in jobs"
        To use Guppy in a job, just reference the commands using their full path. e.g. to use `guppy_basecaller` that you expanded into `/data/gpfs/projects/punim0001/guppy/ont-guppy`, add the following to your Slurm submit script

        ```
        /data/gpfs/projects/punim0001/guppy/ont-guppy/bin/guppy_basecaller
        ```

    === "Common errors"
        ```
        /data/gpfs/projects/punim0001/guppy/ont-guppy/bin/guppy_barcoder: error while loading shared libraries: libcuda.so.1: cannot open shared object file: No such file or directory
        ```

        You receive this error if you run the GPU version of Guppy on a CPU node. Either run it on the GPU partitions, or use the CPU version of Guppy.

