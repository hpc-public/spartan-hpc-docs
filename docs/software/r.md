## R libraries
R's extensions are known as packages. A lot of these have been installed by the Spartan HPC sysadmins when the main package itself is installed. To check what packages are available, load the R module and enter that environment. Then run the command to display the available packages.

```
$ module load foss/2022a R/4.2.2
$ R

$ R

R version 4.2.2 (2022-10-31) -- "Innocent and Trusting"
Copyright (C) 2022 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)

> installed.packages(lib.loc = NULL, priority = NULL, noCache = FALSE, fields = NULL, subarch = .Platform$r_arch)
```

Note that the packages available will differ for each version of R. 

However, sometimes a local (personal) library is required. A library is a collection of R packages. Create a directory for the local R library, invoke R, and install packages to that directory. 

For example:

```
$ mkdir -p ~/R_libs/4.2.2
$ module load foss/2022a R/4.2.2
$ R
R version 4.2.2 (2022-10-31) -- "Innocent and Trusting"
..
> install.packages("snow", repos="http://cran.r-project.org", lib="~/R_libs/4.2.2")
..
> q();

$ export R_LIBS_USER=~/R_libs/4.2.2

$ ls ~/R_libs/4.2.2
snow
```

Check to see that the library path has been added correctly. For example;

```
$ R
> .libPaths();
[1] "/home/lev/R_libs/4.2.2"                                               
..
```

## Common packages needed with R

Some R packages require you load additional modules before you can install them using R. One that we know about is `ragg`. 

When you try to install `ragg` (and other packages), sometimes you will get an error:

```
Package harfbuzz was not found in the pkg-config search path.
Perhaps you should add the directory containing `harfbuzz.pc'
to the PKG_CONFIG_PATH environment variable
No package 'harfbuzz' found
Package fribidi was not found in the pkg-config search path.
Perhaps you should add the directory containing `fribidi.pc'
to the PKG_CONFIG_PATH environment variable
No package 'fribidi' found
Using PKG_CFLAGS=
Using PKG_LIBS=-lfreetype -lharfbuzz -lfribidi -lpng
--------------------------- [ANTICONF] --------------------------------
Configuration failed to find the harfbuzz freetype2 fribidi library. Try installing:
 * deb: libharfbuzz-dev libfribidi-dev (Debian, Ubuntu, etc)
 * rpm: harfbuzz-devel fribidi-devel (Fedora, EPEL)
 * csw: libharfbuzz_dev libfribidi_dev (Solaris)
 * brew: harfbuzz fribidi (OSX)
If harfbuzz freetype2 fribidi is already installed, check that 'pkg-config' is in your
PATH and PKG_CONFIG_PATH contains a harfbuzz freetype2 fribidi.pc file. If pkg-config
is unavailable you can set INCLUDE_DIR and LIB_DIR manually via:
R CMD INSTALL --configure-vars='INCLUDE_DIR=... LIB_DIR=...'
```

To fix it, load the fribidi and harfbuzz modules before starting R. i.e.

```
module load foss/2022a R/4.2.2
module load FriBidi/1.0.12 HarfBuzz/4.2.1
R
```

then try installing your packages again

