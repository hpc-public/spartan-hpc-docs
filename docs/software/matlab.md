In a job MATLAB can be invoked with a particular script using `matlab -nodisplay -nodesktop -r "run my_script.m"`. You may need to add particular working directories so MATLAB can find all the scripts necessary for your job.

To list the available MATLAB toolboxes use the "lstoolboxes" command to list the MATLAB toolboxes that are available (i.e. licensed) for use.

```
$ module load MATLAB/2023a_Update_1
$ matlab
>> ver
```

!!! warning
    === "License checkout failed message and use on login nodes"
        Note that because MATLAB is particularly resource-heavy, it is prohibited to load it on the login nodes. 

        If you try to run MATLAB on the login nodes, you will receive a license checkout failed message when starting MATLAB.  

        You must run MATLAB in a job submitted to the batch system or in an sinteractive session.

To install packages for your own environment, follow the instructions from MathWorks on [Get and Manage Add-Ons](https://www.mathworks.com/help/matlab/matlab_env/get-add-ons.html). See also PACKAGES on Spartan at `/apps/examples/MATLAB/PACKAGES`.

## Parallel processing using parpool

MATLAB offers a parallel computing function, parpool, which can parallelise certain operations. More info can be found at [https://www.mathworks.com/help/parallel-computing/parpool.html](https://www.mathworks.com/help/parallel-computing/parpool.html).  

To use it, we need to add it to our MATLAB code, and make a Slurm file.

!!! example
    === "Example Slurm submit file"
        An example Slurm submit file is

        ```
        #!/bin/bash
        #SBATCH --cpus-per-task=8
        #SBATCH --time=01:00:00

        module load MATLAB/2023b_Update_7

        export SCRATCH="/tmp/${SLURM_JOB_ID}_${SLURM_ARRAY_TASK_ID}"
        mkdir -p $SCRATCH
        matlab -nodesktop -nosplash < test-parpool.m
        ```

        In this example, we are asking for 8 CPUs, which would then allow MATLAB to create a parpool of 8 workers. We set the env variable SCRATCH to a temp directory, which then gets used inside the MATLAB script.

    === "MATLAB code"
        Add the following to your MATLAB .m file (in our example, we'll call it `test-parpool.m`) to create a parpool.

        This example creates a parcluster, sets the JobStorageLocation to '/tmp' so you can run more than 1 job at the same time, then creates a parpool with the parcluster.

        ```
        pc = parcluster('local')
        pc.JobStorageLocation = getenv('SCRATCH')
        parpool(pc, str2num(getenv('SLURM_CPUS_PER_TASK')))
        ```

        For information about what parpools can do, see [https://hpc.nih.gov/apps/Matlabdct.html](https://hpc.nih.gov/apps/Matlabdct.html)

## Known issues with MEX files

As our operating system, RHEL9, is very new, there is currently a compatibility issue with a library distributed with MATLAB.  

If you get an error like  

```
Invalid MEX-file XXX: /apps/easybuild-2022/easybuild/software/Core/MATLAB/2023a_Update_1/bin/glnxa64/../../sys/os/glnxa64/libstdc++.so.6: version `GLIBCXX_3.4.29' not found
```

then you are affected. To fix it, run MATLAB with

```
LD_PRELOAD=/usr/lib64/libstdc++.so.6 matlab
```

This will force the loading of the operating system libstdc++ library instead of the one distributed by MATLAB  
