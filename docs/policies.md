# Policies

## Access Policy

Usage of Spartan, as with all Research Computing Services services, is governed by [Research Computing Services Terms of Service](https://gateway.research.unimelb.edu.au/__data/assets/pdf_file/0009/4672872/RCS-TOS-pub.pdf).  

## Service Denial

Under certain circumstances, access may be restricted or terminated.  These include:

1. Inappropriate use of the service
If a user engages in activity that contravenes the University IT policies, standards and guidelines, or the processes described here, their access to the service will be suspended, pending a review.  In the case where the user is also the project owner, the project may also be suspended, effectively denying access to other project participants.  If the activity is deemed to be malicious, the user and the project may be removed from the service.
2. Damage to the service
If a user misuses the service, either wittingly or unwittingly, and causes the degrading or denial of service for other users, access will be suspended pending a review.  The user will be notified of the problem, and subject to the discretion of the support staff, their access will be reinstated.  Should the problem reoccur, the user account will be suspended again and the user will be contacted by support staff.  Upon review by support staff and approval of the Service Owner, the user's access will be either:
    * Reinstated if the review concludes that the user no longer presents a threat to the system
    * Removed if the review concludes the user is unable to correct the problem, and effectively demonstrates Inappropriate use of the service (see above).


## Keeping account information up to date

All users must keep their account information up to date, including their supervisor, position and department. A valid, institutional email address is required. To update your information, go to [Karaage](https://dashboard.hpc.unimelb.edu.au/karaage), and click Personal->Edit personal details

## Locking of user accounts

User accounts will be locked when emails sent receive a bounce message, indicating the mailbox does not exist anymore.

User home directories will be deleted 6 months after being locked.

## Expiration of the Project

A project will be considered to be expired when there are no valid unlocked user accounts left in the project.  

As per our [Managing Data](https://dashboard.hpc.unimelb.edu.au/data_management/), you should only keep computational data on Spartan. Any critical or not used data should be uploaded to Mediaflux or another platform.  

With no valid unlocked user accounts left in the project, the data in project and scratch is inaccessible to all users.  

We will delete all project and scratch data for the project 6 months after the project has expired according to the above criteria. We will attempt to send an email to the project leader 1 week before we delete the data.  

## Running of applications on the login node

No applications other than code editors (vim, emacs), data moving applications (e.g. rsync, unimelb-mf-clients) and Python/R package installations (e.g. pip install, install.packages()) can be run on the login nodes. Applications using CPU or memory will be killed when run on the login nodes.

For code editors which connect externally, such as VSCode, do not run code on Spartan through those external code editors (e.g. running python code on the login nodes).

## Usage of applications which use TCP ports in batch jobs

Some popular applications (such as Jupyter, RStudio, wandb) require TCP ports to be opened during the running of the application. This could be either for correct running of the application, or require you to access it via a web browser.

There are a few guidelines to follow regarding this:

- No ports are externally accessible on either the login nodes or compute nodes. 

- Do not run applications opening TCP ports on the login nodes.

- As Spartan is a shared resource, you cannot guarantee that a port you want to use won't be already in use by other users on the worker node you are using. You should check to see if the port is available before you try to use it. If you try to open a port that someone else is using, your job will most likely fail. We have put an example script in /apps/examples/TCP_Ports on Spartan.

- If you can access a port, so can everyone else on the platform. That means that others would be able to connect and access the data available through the port you open. Make sure you secure the application using the port with a username/password.

- Try not to use popular ports or ports you don't have access to (< 1024, 8080, 8888)

- If an application is available as part of Open OnDemand, please run it there in preference to other ways of running it.

- Spartan is designed and optimised for batch jobs (i.e. no need for user interaction). As such, it is not really designed for applications which require users to connect via web interfaces. It may be very difficult or impossible to make certain applications work with Spartan. 

- We will not host web applications which need to be always on. If you require this, please consider using the Melbourne Research Cloud.

