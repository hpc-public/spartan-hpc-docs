---
title: Private partition specifications
---
## Deeplearn

The deeplearn partition has been bought for use by Computing and Information Systems research staff and students

| Node name              | Nodes | Cores/node | Memory/node (MB) | Processor                                   | GPU Type | GPU Memory                      | Slurm node type                    |
|------------------------|-------|------------|-------------|---------------------------------------------|---------------|------------------|------------------------------------|
| spartan-gpgpu[091-096] | 6     | 32         | 234000       | Intel(R) Xeon(R) Gold 6326 CPU @ 2.90GHz    | A100 | 40GB GPU RAM per GPU | dlg4                               |
| spartan-gpgpu[065-071,132-143,160,166-169] | 24     | 32         | 1000000       | Intel(R) Xeon(R) Gold 6326 CPU @ 2.90GHz    | A100 | 80GB GPU RAM per GPU | dlg5                               |
| spartan-gpgpu170       | 1     | 64         | 975000       | Intel(R) Xeon(R) Platinum 8462Y+    | H100 | 80GB GPU RAM per GPU | dlg6                               |

!!! note
    The deeplearn partition is available to specific Computing and Information Systems projects only. You can request access to it at the time of creating a Spartan account, or by submitting a help ticket.

To access the deeplearn partition:

```
#SBATCH --partition=deeplearn
#SBATCH --qos=gpgpudeeplearn
```

To specify that your job should only run on a specific node type, add a constraint to your submission script. e.g. to specify the V100SXM2 nodes, add `#SBATCH --constraint=dlg3` to your submit script.

## feit-gpu-a100

The feit-gpu-a100 partition has been bought for use by FEIT research staff and students.

| Node name              | Nodes | Cores/node | Memory/node (MB) | Processor                                   | GPU Type | GPU Memory                      | Slurm node type                    |
|------------------------|-------|------------|-------------|---------------------------------------------|---------------|------------------|------------------------------------|
| spartan-gpgpu[144-159,161-165] | 21     | 32         | 1000000       | Intel(R) Xeon(R) Gold 6326 CPU @ 2.90GHz   | A100 | 80GB GPU RAM per GPU     |                                |

!!! note
    The feit-gpu-a100 partition is available to FEIT projects only. You can request access to it at the time of creating a Spartan account, or by submitting a help ticket.

To access the feit-gpu-a100 partition:

```
#SBATCH --partition=feit-gpu-a100
#SBATCH --qos=feit
```

## fos-gpu-l40s

The fos-gpu-l40s partition has been bought for use by Faculty of Science research staff and students.

| Node name              | Nodes | Cores/node | Memory/node (MB) | Processor                                   | GPU Type | GPU Memory                      | Slurm node type                    |
|------------------------|-------|------------|------------------|---------------------------------------------|----------|---------------------------------|------------------------------------|
| spartan-gpgpu[181-192] | 12    | 64         | 950000           | INTEL(R) XEON(R) PLATINUM 8562Y+            | L40S     | 48GB GPU RAM per GPU            |                                    |

!!! note
    The fos-gpu-l40s partition is available to Faculty of Science projects only. You can request access to it at the time of creating a Spartan account, or by submitting a help ticket.

To access the fos-gpu-l40s partition:

```
#SBATCH --partition=fos-gpu-l40s
#SBATCH --qos=fos
```

## fos

The fos partition has been bought for use by Faculty of Science research staff and students.

| Node name              | Nodes | Cores/node | Memory/node (MB) | Processor                                   | Slurm node type                    |
|------------------------|-------|------------|------------------|---------------------------------------------|------------------------------------|
| spartan-bm[182-185]    | 4     | 128        | 2001000          | Intel(R) Xeon(R) Gold 6448H                 | avx512, sr                         |

!!! note
    The fos partition is available to Faculty of Science projects only. You can request access to it at the time of creating a Spartan account, or by submitting a help ticket.

To access the fos partition:

```
#SBATCH --partition=fos
#SBATCH --qos=fos
```
