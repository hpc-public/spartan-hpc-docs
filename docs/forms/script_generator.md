# Job script generator for Slurm
This page generates job scripts for Spartan, which uses the <a href="https://slurm.schedmd.com/">Slurm</a> workload manager. It isn't intended to cover every option available, just a little help to get you started with some of the possibilities and how the syntax works.

Enter the details of your job in the form below and then click on the <a href="#makescript">make script</a> button below.

This utility is adapted from work by Bernie Pope, Ben Moran and Lev Lafayette.

<div class="scriptgen">
<form>

<div>
  <label for="name">Job Name: </label>
  <input id="name" type="text" class="form-control">
  <p class="form-text text-muted">
    It is a good idea to give your job a meaningful name. This will help you identify it when listing the job queue.
  </p>
</div>

<div>
  <label for="projectid">Project ID: </label>
  <input id="projectid" type="text" class="form-control">
  <p class="form-text text-muted">
    Specify the project ID to which this job should be attributed. For example, punim0001.
  </p>
</div>

<div>
  <label for="partition">Partition: </label>
  <select id="partition">
    <option value="sapphire">sapphire</option>
    <option value="cascade">cascade</option>
    <option value="long">long</option>
    <option value="bigmem">bigmem</option>
    <option value="gpu-a100">gpu-a100</option>
    <option value="gpu-h100">gpu-h100</option>
  </select>
</div>

<div>
  <label for="projectid">Job Type: </label>
  <select id="jobtype">
    <option value="single" selected="selected">Single Core</option>
    <option value="multithreaded">Multithreaded (SMP)</option>
    <option value="mpi">MPI, single or multiple node</option>
  </select>
</div>
<div id="jobtype-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      </div><!-- modal-header -->
      <div class="modal-body">
        <strong>Single Core</strong>
        <p>Suitable for single CPU core jobs only (not parallel).</p>
        <strong>Multithreaded (SMP)</strong>
        <p>Suitable for shared-memory multithreaded jobs. <em>Note</em> these jobs cannot use more than one node and cannot use more cores than the number of cores on a node. </p>
        <strong>MPI</strong>
        <p>Suitable for distributed parallel jobs that use MPI. May use more than one cluster node. These are allocated to the "cascade" or "sapphire" partition.</p>
        <strong>Notes</strong>
        <p>If the job submission is more complex that these deliberately restrictive criteria (e.g., SMP jobs with more than 12 cores, MPI jobs with specific node allocations, GPU partition and GRES requirements), then you should be at the stage of writing your own scripts!</p>
      </div><!-- modal-body -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div><!-- modal-footer -->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- jobtype-modal -->

<p class="form-text text-muted">
  Specify whether your job needs one or more cores <a data-toggle="modal" data-target="#jobtype-modal" href="#">(more...)</a>
</p>

<div>
  <label for="cores">CPU Cores (total): </label>
  <input id="cores" type="text" class="form-control" value="1">
  <p class="form-text text-muted">
    The maximum number of CPU cores needed by your job.
  </p>
</div>

<div>
  <label for="gpus">GPUs (per node): </label>
  <input id="gpus" type="text" class="form-control" value="0">
  <p class="form-text text-muted">
    The number of GPUs you would like, per node. If you selected the partition gpu-a100 or gpu-h100, please ensure this value is more than 0
  </p>
</div>

<div>
  <label for="qos">Slurm QoS: </label>
  <input id="qos" type="text" class="form-control" value="">
  <p class="form-text text-muted">
    The Slurm QoS for the job.
  </p>
</div>

<div>
  <label for="memory">Required Memory (GB): </label>
  <input id="memory" type="text" class="form-control">
</div>
<div id="memory-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      </div><!-- modal-header -->
      <div class="modal-body">
        <strong>Single Core and MPI</strong>
        <p>For Single Core and MPI jobs this specifies the amount of memory allocated to each CPU core requested by the job. The total memory used is equal to the number of cores in the job multiplied by the amount of memory requested. If you leave this blank then your job will be allocated 4000 megabytes per CPU core.</p>
        <p>For example if you request 2 CPU cores and 8GB of memory per core, the total memory used by the job is 16GB.</p>
        <strong>Multithreaded (SMP) jobs</strong>
        <p>For Multithreaded (SMP) jobs this is the total memory allocated per node, irrespective of the number of CPU cores.</p>
      </div><!-- modal-body -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div><!-- modal-footer -->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- memory-modal -->

<p class="form-text text-muted">
 Leave this blank to use the default <a data-toggle="modal" data-target="#memory-modal" href="#">(more...)</a>
</p>

<div>
  <label for="emailaddress">Notification Email: </label>
  <input id="emailaddress" type="text" class="form-control" >
  <input id="emailjobstart" type="checkbox" /> Job Starts<br />
  <input id="emailjobend" type="checkbox" /> Job Ends (Success)<br />
  <input id="emailjobdie" type="checkbox" /> Job Ends (Error)<br />
  <p class="form-text text-muted">
    You can be notified by email when your job starts running or when it ends (either successfully or with an error).
  </p>
</div>

<div>
  <label for="days">Walltime: </label>
  <input id="days" type="text" value="0" class="form-control" /> days
  <input id="hours" type="text" value="1" class="form-control" /> hours
  <input id="minutes" type="text" value="0" class="form-control" /> minutes

  <p class="form-text text-muted">
    Maximum amount of time needed for your job to complete (it will be terminated after this time).
  </p>
</div>

<div>
  <label for="workdir">Directory to run the job from:</label><br>

  <div>
    <input checked aria-required="true" id="workdir" value="workdir" name="directory" type="radio" value="yes" data-error="Please make a selection" />
    <label for="workdir">
      <span>The same directory where it is launched</span>
    </label>
  </div>
  <div>
    <input aria-required="true" id="homedir" value="homedir" name="directory" type="radio" value="yes" data-error="Please make a selection" />
    <label for="workdir">
      <span>Home directory</span>
    </label>
  </div>
  <div>
    <input aria-required="true" id="otherdir" value="otherdir" name="directory" type="radio" value="yes" data-error="Please make a selection" />
    <label for="workdir">
      <span>Other</span>
    </label>
    <input id="otherdirname" type="text" class="form-control"/>
  </div>
</div>

<div id="dir-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      </div><!-- modal-header -->
      <div class="modal-body">
        <p>Each job is run from a given directory on the computer's filesystem - this is called the <em>working directory</em> in Unix terminology.</p>
        <p>You need to set the working directory correctly so that your job can find its input files and generate its output files in the appropriate location. In most cases it is desirable to set this to the directory where the job was launched, but you might also like it to be your home directory, or from some other specific directory on the computer.</p>
        <p>If you specify the directory, it must be an absolute reference (i.e. /home/foo/mydir) or relative to the launch directory.</p>
      </div><!-- modal-body -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div><!-- modal-footer -->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- dir-modal -->

<p class="form-text text-muted">
  The working directory for your job <a data-toggle="modal" data-target="#dir-modal" href="#">(more...)</a>
</p>

<div>
  <label for="modules">Modules:</label>
  <textarea id="modules" type="textarea" class="form-control" ></textarea>
</div>
<div id="modules-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      </div><!-- modal-header -->
      <div class="modal-body">
        <strong>Single Core and MPI</strong>
        <p>The <em>modules</em> utility sets up your environment paths for particular versions of specified programs. It is possible to use more than one module in your job (just list all the ones you need on separate lines).</p>
        <p>For example to use version <code>3.2</code> of the application <code>foo</code>, which was compiled with <code>gcc</code> version 4.9.2, you should load the module called: <code>foo/3.2-gcc-4.9.2</code></p>
        <p>You can list all available modules on Spartan using the <code>module avail</code> command.</p>
      </div><!-- modal-body -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div><!-- modal-footer -->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modules-modal -->

<p class="form-text text-muted">
   List of modules to load for your job, one per line <a data-toggle="modal" data-target="#modules-modal" href="#">(more...)</a>
</p>


<div>
  <label for="command">Command: </label>
  <textarea id="command" type="text" value="0" class="form-control" ></textarea>
  <p class="form-text text-muted">
    Enter the command (or commands) that you want to run, one per line. Note that MPI jobs need to prefix the command with <em>srun</em>.
  </p>
</div>

<div>
  <input class="button-primary" id="makescript" type="button" value="Make Script" />
</div>

<div class="row" id="resultswrapper">
  <div id="jobscript">
    <!-- output to be inserted here -->
  </div><!-- jobscript -->
</div><!-- resultswrapper -->

<div class="row" id="downloadsection" style="display:none">
  <div class="twelve columns" id="downloadlink">
    <a class="button button-primary" id="downloadanchor" download="sbatch-script.txt" href="">Download this script</a>
  </div><!-- downloadlink -->
</div><!-- downloadsection -->

</form>
</div><!-- scriptgen -->

<script type="text/javascript" src="slurm.js"></script>
