!!! example

    === "Basic usage"
        To use system Jupyter, select "Jupyter 3.5.0"

        [![](/images/Jupyter_modules_ood.png "Jupyter Modules")](/images/Jupyter_modules_ood.png)

    === "Use Juypter with a python virtualenv"

        To use Jupyter with a [Python virtualenv](/software/python/)  
          - On the login node, select a python version to use, activate your virtualenv and install the jupyter and jupyterlab modules using pip - e.g.  
            ```
            module load foss/2022a Python/3.10.4
            source ~/venvs/3.10.4/bin/activate
            pip install jupyterlab notebook
            ``` 
          - In Open OnDemand, select the "Modules" to "Virtual Python 3.10.4", and put `source ~/venvs/3.10.4/bin/activate` to the "Commands to run before Jupyter"

        [![](/images/Jupyter_modules_ood_virtual.png "Jupyter Python")](/images/Jupyter_modules_ood_virtual.png)

    === "Use Jupyter with Anaconda"

        To use Jupyter with Anaconda  
          - On the login node, unload the existing modules, load the Anaconda module, activate your conda env, and install the Jupyter and Jupyterlab modules using pip - e.g.
            ```
            module purge
            module load Anaconda3/2024.02-1
            eval "$(conda shell.bash hook)"
            conda activate Your_Conda_Environment
            pip install jupyterlab notebook
            ```
          - Select "Anaconda 2024.02-1" in the Modules section in OnDemand
          - Put `conda activate Your_Conda_Environment` in "Commands to run before Jupyter" section.

        [![](/images/Jupyter_modules_ood_conda.png "Jupyter Conda")](/images/Jupyter_modules_ood_conda.png)

    === "Use Jupyter with a GPU"

        To use Jupyter with GPU, select "Jupyter/3.5.0" in the Modules section, put "module load CUDA/11.7.0" in "Commands to run before Jupyter" section, and change the "Number of GPUs" to be 1 or more

        [![](/images/Jupyter_modules_ood_gpgpu1.png "Jupyter Modules GPGPU1")](/images/Jupyter_modules_ood_gpgpu1.png)

        [![](/images/Jupyter_modules_ood_gpgpu2.png "Jupyter Modules GPGPU2")](/images/Jupyter_modules_ood_gpgpu2.png)

    === "Integrate a python venv with OOD jupyter"

        Login to spartan via ssh and activate your existing virtual env (venv-3.10.4)

        Install ipykernel pip package
        ```
        $ pip install ipykernel
        ```

        Now integrate that with jupyter notebook. You can choose a different kernel name, but I just went with the same name "venv"
        ```
        $ python -m ipykernel install --user --name=spartan-venv
        ```

        NOTE: some users may end up needing to install ipyparallel package as well in their venv


        Check the list of kernels. We now have "venv" also listed
        ```
        $ ls ~/.local/share/jupyter/kernels/
        ```

        The ~/.local/share/jupyter/kernels/spartan-venv/kernel.json points to the python executable inside your vitual environment. Jupyter will automatically pick this up when you select the "spartan-venv" option.
        ```
        $ cat ~/.local/share/jupyter/kernels/spartan-venv/kernel.json
        ```

        In opendemand, create a jupyter notebooks session with the following input values:

        Add the dependency "Modules" field to ensure that the right python modules are loaded.


        Remove the activate command from "Commands to run before Jupyter"

        Start the jupyter session and select the spartan-venv from the available kernels and start a new notebook.
        You may select "spartan-venv" by clicking the drop-down-box (labeled as "New") near the top left corner to open a new notebook.
        For existing notebooks, you may change the kernel by selecting "Kernel -> > Change Kernel -> spartan-venv"
