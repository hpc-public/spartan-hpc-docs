
!!! example

    === "RStudio with R from software system"

        * Select "RStudio-Server/2022.07.2+576-Java-11-R-4.2.1" to use RStudio Server with R/4.2.1 from the software system.  

        [![](/images/Rstudio_module.png "RStudio from Module system")](/images/Rstudio_module.png)

        * Modify the partition, number of hours, CPU cores and memory options as required.  

        Note: Do not use this option if you want to use R from Anaconda. Select the Anaconda version instead.

        * OnDemand RStudio allows users to load extra modules if needed. To do this, add the modules you want to load in the "Load extra module" section. eg rgdal/1.6-6

        [![](/images/R_extra_modules_ood.png "R extra modules")](/images/R_extra_modules_ood.png)

    === "RStudio with R from Conda environment"

        * Select "RStudio-Server/2022.07.2+576-Java-11 and Anaconda3/2024.02-1 - For Conda environments" to use RStudio Server with R from a Conda environment.  

        [![](/images/Rstudio_conda.png "RStudio from Conda")](/images/Rstudio_conda.png)

        This option will launch RStudio, using R from a Conda environment. Before launching, make sure you install your required R version in the Conda environment.  

        e.g.

        ```
        module load Anaconda3/2024.02-1
        eval "$(conda shell.bash hook)"
        conda activate seantestenv
        conda install -c conda-forge r-base openldap
        ```

        Note: Do not use this option if you want to use modules from the software system. Select the Module option instead.  

        New (2024/09/16): Added info about installing openldap in the Conda env. It appears new versions of R require you to install openldap in the Conda env.  

        * Enter your Conda env name in the "Conda environment to load" textbox.

        * Modify the partition, number of hours, CPU cores and memory options as required.         

    === "Known issues"

        * Setting the working directory on startup in RStudio does not work as expected. A recommended workaround is to `setwd` in RSTudio, and saved the start-up directory from RStudio in global options.
