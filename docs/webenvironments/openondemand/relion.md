!!! example

    === "Basic usage"
        To use Relion version 5, select 5.0 from Relion-5.0.

        For most cases, Relion GUI doesn't need a GPU. Users can submit jobs to the GPU partition throught Relion GUI.

        User can to submit a job via Relion GUI with the following options:

        "Submit to queue" --> Yes

        For gpu partition job

        "Standard submmission script" --> /apps/easybuild-2022/easybuild/software/MPI/intel/2022.1.0/OpenMPI/4.1.4/Relion/5.0/scripts/slurm_gpu.sh

        For cpu partition job

        "Standard submmission script" --> /apps/easybuild-2022/easybuild/software/MPI/intel/2022.1.0/OpenMPI/4.1.4/Relion/5.0/scripts/slurm.sh

        [![](/images/Relion_job_submission.png "Relion Job Submission")](/images/Relion_job_submission.png)

        Please see [Relion](/software/relion) for more Relion information.

