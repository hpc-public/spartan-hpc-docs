!!! example

    === "Prerequisites"

        To start Cryosparc, first you need to have a license ID. You need a license ID for every unique instance of Cryosparc you start.

        You don't need multiple license ID's if you only have one Cryosparc session running at a time. To obtain a license ID, go to [Cryosparc](https://cryosparc.com/download)

    === "Basic usage" 

        Go to [Open OnDemand](https://spartan-ood.hpc.unimelb.edu.au/pun/sys/dashboard/batch_connect/sys/ood-cryosparc/session_contexts/new)

        - Enter your License ID in the License ID box.

        - Choose number of hours - it must be long enough for all the Cryosparc jobs to finish

        - Choose number of CPU cores - 8 is a good start

        - Choose RAM - 64 is a good start

        - Choose number of GPUs - 1 is probably fine for most people

        - Then press Launch

        [![](/images/cryosparc.png "Logo Title Text 1")](/images/cryosparc.png)

        A Cryosparc master and worker will be launched at the same time. They will run in the same session and on the same node.

        *NB* You can only run a single Cryosparc at a time. This is because it will use a Cryosparc DB in a specific location in your home directory, and you can't run multiple Cryosparc instances all using the same database. If you need to run multiple Cryosparcs at the same time, please contact HPC Support.

        You will see a blank Linux desktop.

        - Launch Firefox. Click Applications->Internet->Firefox

        - For Cryosparc browse to http://localhost:39000

        - Use the email address and password you set

    === "Mounting Mediaflux"

        Please follow the guide to [Mounting Mediaflux](https://dashboard.hpc.unimelb.edu.au/webenvironments/openondemand/virtualdesktop/#__tabbed_1_2)

        Run the following command to reload Apptainer image:
	
        ```
        module load foss/2022a

        module load Apptainer/1.1.8

        export CRYOSPARC_MASTER_PATH=/app/cryosparc_master

        export CRYOSPARC_WORKER_PATH=/app/cryosparc_worker

        export CRYOSPARC_DATADIR=$HOME/cryosparc 

        export CRYOSPARC_LICENSE_ID=YOUR_LICENSE_ID

        apptainer instance start -B /apps,/data,/tmp,/tmp/run:/run,$CRYOSPARC_DATADIR/config.sh:$CRYOSPARC_MASTER_PATH/config.sh,$CRYOSPARC_DATADIR/run:$CRYOSPARC_MASTER_PATH/run,$CRYOSPARC_DATADIR/config.sh:$CRYOSPARC_WORKER_PATH/config.sh --nv /apps/singularity_images/Cryosparc/cryosparc-4.4.1-rhel9.img cryosparc

        apptainer exec instance://cryosparc /cryosparc.sh

        ```

        Open firefox browse to localhost:39000

        Once you finish, remember to stop the Apptainer by running the command:
	
        ```
        apptainer instance stop cryosparc
	
        ```

