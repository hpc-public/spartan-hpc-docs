Virtual Desktop in Open OnDemand allows you to start an XFCE desktop session on one of the worker nodes. You can choose to have a standard session, or one running using a GPU.

You may want to run on a GPU node if you are using graphics heavy applications, such as Drishti

!!! example

    === "Basic usage"

        [![](/images/virtual_desktop_ood.png "Virtual Desktop")](/images/virtual_desktop_ood.png)

        Select the partition you wish to run on, the number of CPU cores, the memory and the amount of time. Then click Launch. Once the job starts running, you can then click "Launch Virtual Desktop" to get your virtual desktop in the browser.

    === "Mounting Mediaflux"

        If you are using a Virtual Desktop session, you may want to mount mediaflux in your Virtual Desktop, so you can access your Mediaflux files in applications run in your Virtual Desktop

        Please run the following commands in a Terminal:

        ```
        mkdir /tmp/gvfs-$(id -u)

        chmod 700 /tmp/gvfs-$(id -u)

        export XDG_RUNTIME_DIR=/tmp/gvfs-$(id -u)

        dbus-run-session bash

        gio mount smb://mediaflux.researchsoftware.unimelb.edu.au/proj-<Mediaflux project ID>

        ```
        Then you need to put in:

        ```
        User [Username]: Your_Uni_Username

        Domain [SAMBA]: unimelb

        Password: Your_Uni_Password

        ```
        Check if you have mounted mediaflux:

        ```
        ls /tmp/gvfs-$(id -u)/gvfs/'smb-share:server=mediaflux.researchsoftware.unimelb.edu.au,share=proj-<Mediaflux project ID>'

        ```

        You can then use the path `/tmp/gvfs-$(id -u)/gvfs/'smb-share:server=mediaflux.researchsoftware.unimelb.edu.au,share=proj-<Mediaflux project ID>'` in your interactive application as a location where your Mediaflux files are

