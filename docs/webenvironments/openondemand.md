Open OnDemand is a HPC portal that provides web access to HPC resources, such a file management, command-line access, job management, and access to certain web-enabled applications (e.g., RStudio, Jupyter Notebooks). To access OpenOnDemand one needs to create a session from a Spartan login website with the user name and password.

[Login to Open OnDemand](https://spartan-ood.hpc.unimelb.edu.au/pun/sys/dashboard/batch_connect/sessions)

The following shows an example opening screen.

[![](/images/openondemand1.png "Open OnDemand Opening Screen")](/images/openondemand1.png)

Most of the menu items are self-explanatory. The "Files" menu item provide a GUI interface to a user's home directory. The "Jobs" menu has two parts, an "Active Jobs" application and a "Job Composer". The "Clusters" menu item provides an application for command-line access to Spartan in a web-browser. The "Interactive Apps" item provides links, replicated on a left hnad menu item, to a Jupyter Notebook server and an RStudio Server. In addition, there is a image for managing your interactive sessions. a help icon, a user icon, and a log out menu item.

!!! example

    === "Files"

        The Open OnDemand File Explorer application provides a graphical representation of your home directory with a tree structure on the left side and file names, human readable file size and modification date on the right main division. Content can be navigated in the browser with the right main division changing accordingly.  A top menu division allows for directory navigation ("Go To"), a session to be opened in a browser-based terminal emulator, the creation of new files and directories, the capability to upload files, and options to show hidden files ("dotfiles") and the the file ownership and mode.

        Files may be viewed, edited, renamed/moved, downloaded, and deleted. Because the session is running in the web-browser whatever file applications associations one has with the browser will be used on file. This is especially relevant for files in a binary format (.pdf, .docx, etc). For text files, Open OnDemand has its own viewer and editor in the browser. Note that the deletion rules are the same as being on the cluster; deleted is gone!

        Multiple files can be selected with shift and cursor navigation (either mouse-click or arrow keys), and there is an menu option for select/unselect all.

    === "Jobs"

        The Job Composer allows for the creation of some jobs with simple options, along with submitting such jobs, stopping them, and deleting them. The latter three are indicated by the play, stop, and trashcan icons. A new job can be composed from a default template or a specified path. Jobs will be in a OnDemand specified directory by default e.g., `/home/lev/ondemand/data/sys/myjobs/projects/default/1`

        [![](/images/openondemand2.png "Open OnDemand File Explorer")](/images/openondemand2.png)

        Jobs can be edited with the Open OnDemand editor, and this will almost certainly be necessary to add modules etc. Consider using the [Job Composer](/forms/script_generator/) or make use of one of the many application-specific templates in `/apps/examples`

    === "Clusters"

        The cluster menu option only has one item, SSH access to Spartan. This will provide a terminal emulator within the browser session. It will also make use of any .ssh config or passwordless SSH that has been previously installed.

    === "Interactive Applications"

        The current interactive apps on Spartan's Open OnDemand are Virtual Desktop, Jupyter Notebook, RStudio-Server, Paraview, Matlab, Tensorboard, Relion and Cryosparc. These work by starting up an interactive job on Spartan and forwarding the display to the web browser session.

        [![](/images/applications_ood.png "Open OnDemand Applications")](/images/applications_ood.png)

        When starting these interactive apps some parameters need to be selected or entered to start the session.

        For example, select version from the drop down menu on application page:

        [![](/images/MATLAB_dropdown_ood.png "MATLAB dropdown")](/images/MATLAB_dropdown_ood.png)


    === "Some known issues"

        Many operations will with Open OnDemand will be somewhat slower than direct access to Spartan as there is some overhead involved. However, there is some additional issues to be aware of.

        * Filenames or directories with <a href="https://github.com/OSC/ood-fileexplorer/issues/198">XML or HTML</a> special characters (<. >, &) may cause issues with the display, resulting in an unusuable File Explorer application.

        * Uploading very large files (that exceed the capacity of the temp directory) may fail or send a partial file without reporting an error.

        In all these cases it is recommended using a system that interacts with the file system directly (e.g., scp, rsync, FileZilla, WinSCP, etc)

        * Opening larger files (multiple megabyte) in the file explorer can cause the right click to fail in Firefox.

        * Attempting to open the tree's root menu in the File Explorer with a right-click in a new tab causes a navigation error.


