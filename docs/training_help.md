## Contacts

If you require support from the HPC Team, please contact us using the following forms
 
__**To request new software or changes to quota:**__

* UoM Staff: [http://go.unimelb.edu.au/ku78](https://go.unimelb.edu.au/ku78)  

* UoM Student: [http://go.unimelb.edu.au/7r96](https://go.unimelb.edu.au/ao78)  
 
__**To report issues:**__
 
* UoM Staff: [https://go.unimelb.edu.au/3u78](https://go.unimelb.edu.au/3u78)  

* UoM Student: [https://go.unimelb.edu.au/xo78](https://go.unimelb.edu.au/xo78)  
 
__**Note - for external users, please email hpc-support@unimelb.edu.au

## HPC Support

There is some scoping issues for HPC support. Our resources are limited and, as a result, some guidelines are in place to provide the more optimal results for researchers.

### Basics

* Please read the Message of the Day when you login! [University regulations]([https://dashboard.hpc.unimelb.edu.au/policies/), system upgrades, outages, etc are included in the MotD. 

* Please read relevant documentation including the policies and process that we have on the HPC dashboard (this site), job submission examples in `/apps/examples` and Slurm error messages.

* Don't try to use sudo. Spartan is a shared system and allowing users access to sudo would impact every other user's work in unpredictable ways, as well as being a major security risk.

* Don't try to install software with apt, yum, or similar package managers. Software can be installed system-wide by administrators for use in the environment modules system. Users can install extensions for Python, Perl, R etc by following the relevant instructions in `/apps/examples`.

* Don't email individual system administrators for HPC support. We need consolidated records. Use the ticketing system instead.

* Please be informative about the error or issue that you are facing in your ticket requests. Also, try to ensure you have separate tickets for separate issues. 

* Please listen to the suggestions of HPC sysadmins, especially when expressed in the negative ("don't try this", "this will not work", etc).

* Support for Spartan is offered during normal business hours, i.e., 9am to 5pm Monday to Friday. 

### Scope

The following are items that the HPC Support team will definitely help you with:

* Account and project management.
* Shell commands, standard Linux scripting (e.g., bash, tsch, sed) with emphasis and guidance on user learning.
* Job submission scripts with emphasis and guidance on user learning.
* Building well-supported research open source software and modules.
* Data management with emphasis and guidance on user learning.
* Notifying upstream providers of bug-fixes where we have a support contract.
* Consultation on HPC project planning and data and software workflow.

The following support is subject to time and staff resources.

* Open source software and modules that have Spartan HPC compatibility issues.
* Proprietary software installation and extensions.
* Pre-built workflow stacks, custom applications.
* Pre-built workflow stacks (e.g., Conda environments), custom applications.
* Programming support (awk, perl, python, C/C++, Fortran, etc), patching of open source software.

The following are areas which we cannot help.

* Notifying upstream providers of bug-fixes where we do not have a support contract.
* Users who engage in abusive behaviour.
* Any workflow or activity that is contrary to University policy.
* Any workflow or activity that threaten the integrity of the HPC system.

## Training

The HPC team offers several courses for new and experienced users on a regular basis. These include:

* Introduction to Linux and High Performance Computing
* Advanced Linux and Shell Scripting for High Performance Computing
* Introduction to Parallel Programming
* Introduction to GPU Programming
* Regular Expressions with Linux


We have also run specialist courses for researchers in bioinformatics, mechanical engineering, and neuroscience.

We *strongly* encourage researchers that are new to HPC to undertake training with us. It's free for University of Melbourne researchers. We run regular one-day courses on HPC, shell scripting, parallel programming, GPU programming, and regular expressions. Research Computing Services also offer training in a wide range of other digital tools to accelerate your research. 

We can also tailor a specific training program for you, for instance around a specific software package or discipline-based applications, if there is the demand. For example, we have previously run courses specifically for genomics and mechanical engineering.

Finally, if you ever get stuck, please feel free to [contact HPC support](#contacts). We're here to help make your research more productive and enjoyable, and we'll do everything we can to help. 

## Training Program 2025

The following is the timetable of expected onboarding workshops for 2025

Jan 30 (Thu) Introduction to Linux and Supercomputing
Jan 31 (Fri) Advanced Linux and Shell Scripting for HPC

Feb 24 (Mon) Parallel and High Performance Python
Feb 25 (Tue) Regular Expressions with Linux

Mar 24 (Mon) Parallel Processing on Spartan 
Mar 25 (Tue) Mathematical and Statistical Programming

Apr 14 (Mon) Introduction to Linux and Supercomputing
Apr 15 (Tue) Advanced Linux and Shell Scripting for HPC

May 15 (Thu) From Spartan to Gadi
May 16 (Fri) Data and Databases on HPC

Jun 24 (Tue) Parallel and High Performance Python
Jun 25 (Wed) Parallel Processing on Spartan

Jul 15 (Tue) Introduction to Linux and Supercomputing
Jul 16 (Wed) Advanced Linux and Shell Scripting for HPC

Aug 28 (Thu) Regular Expressions with Linux
Aug 29 (Fri) GPGPU Applications and Programming

Sep 23 (Mon) Mathematical and Statistical Programming
Sep 24 (Tue) Parallel and High Performance Python

Oct 15 (Wed) Introduction to Linux and Supercomputing
Oct 16 (Thu) Advanced Linux and Shell Scripting for HPC

Nov 05 (Tue) Parallel and High Performance Python
Nov 06 (Wed) Parallel Processing on Spartan
