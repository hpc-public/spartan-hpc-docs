## Spartan Daily Weather Report
### 20250314
* GPFS usage: 2017.62TB Free: 330.08TB (85%)
* Spartan utilisation on the sapphire partition is 100.00% node allocation, 96.00% CPU allocation.
* Spartan utilisation on the cascade partition is 100.00% node allocation, 96.85% CPU allocation.
* Total queued pending/queued on the public partitions: 6378
* Spartan utilisation on the gpu-a100 partition is 100.00% node allocation. Total queued/pending: 40
* Spartan utilisation on the gpu-h100 partition is 100.00% node allocation. Total queued/pending: 18
* GPU card usage on the gpu-a100 partition is:  112 / 116 cards in use (96.55%)
* GPU card usage on the gpu-h100 partition is:  25 / 40 cards in use (62.50%)
* 1 nodes out.

### Wait Time
*How long will my job take to start?*

<div id="divwaittime" style="width: 85%; margin: 0 auto;"></div>

<div class="container">
  <div class="row" style="width:50%; margin: 0 auto;">
    <div class="col-sm">
      Partition: <select id="partitionSelect" class="alt"></select>
    </div>
    <div class="col-sm">
      CPUs Requested: <select id="coreSelect" class="alt"></select>
    </div>
    <div class="col-sm">
      Wall Time: <select id="wallTimeSelect" class="alt"></select>
    </div>
  </div>
</div>


<br/>

<ul class="accordion">
  <li>
<span class="accordion__title">How to Interpret</span>
<div class="accordion__hidden">
<ul>
<li>This plot provides data on how long previous jobs have taken to start, which can be used as guidance on how long your job might take to start.</li>
<li>Note however that "Past performance is no guarantee of future results"; wait times can fluctuate quickly due to changes in usage or outages, and wait time could be considerably more or less than the historic average.</li>
<li>Daily averages are shown, but points may be missing for days where there were no jobs matching the selected characteristics.</li>
<li>This data is acquired using the <code>sacct</code> command in Slurm to get a list of all recent jobs and their start/end times.</li>
</ul>
</div>
</li>
</ul>


<script>

    window.onload = function() {

        Plotly.d3.json("/jsondata/wait_time.json" + '?' + Math.floor(Math.random() * 1000), function (data) {

            function assignOptions(options, selector) {
                $.each(options, function (val, text) {
                    selector.append(
                        $('<option></option>').val(text).html(text)
                    );
                });
            }

            function updatePlot() {
                var graphData = [
                    {   x: data['data'][wallTimeSelect.val()][partitionSelect.val()][coreSelect.val()]['x'],
                        y: data['data'][wallTimeSelect.val()][partitionSelect.val()][coreSelect.val()]['y'],
                        text: 'hours',
                        mode: 'markers',
                        hoverinfo: 'x+text+y',
                        marker: {
                            size: 10
                        },
                        name: 'Daily Average'
                    },
                    {
                        x: data['data'][wallTimeSelect.val()][partitionSelect.val()][coreSelect.val()]['x'],
                        y: data['data'][wallTimeSelect.val()][partitionSelect.val()][coreSelect.val()]['y_rolling_average'],
                        hoverinfo: 'none',
                        line: {
                            color: 'rgb(150, 150, 150)',
                            width: 1,
                            dash: 'dash'
                        },
                        name: 'Rolling 14d Mean'
                    }
                ];

                var layout = {
                    title: 'Job Wait Time',
                    width: 700,
                    height: 400,
                    yaxis: {
                        title: 'Hours',
                        hoverformat: '.2f hours'
                    },
                    legend: {
                        x: 0.5, 
                        y: 1.2,
                        orientation: 'h',
                        xanchor: 'center',
                    }
                };
                Plotly.newPlot('divwaittime', graphData, layout);
            }

            var partitionSelect = $('#partitionSelect');
            var coreSelect = $('#coreSelect');
            var wallTimeSelect = $('#wallTimeSelect');

            // Populate drop-down options
            assignOptions(data['options']['partitions'], partitionSelect);
            assignOptions(data['options']['cores'], coreSelect);
            assignOptions(data['options']['wall_times'], wallTimeSelect);

            // Attach listeners
            partitionSelect.change(updatePlot);
            coreSelect.change(updatePlot);
            wallTimeSelect.change(updatePlot);

            // Build initial plot
            updatePlot()

        });
    }


</script>


## Specifications

Spartan has a number of partitions available for general usage. A full list of partitions can be viewed with the command `sinfo -s`.

| Partition      | Nodes | Cores per node | Memory per node (MB) | Processor                    | Peak Performance (DP TFlops) | Slurm node types |Extra notes                              |
|----------------|-------|------------|-------------|-------------------------------------------|------------------------------|------------------|-----------------------------------------|
| interactive    | 4     | 128        | 977000      | Intel(R) Xeon(R) Gold 6448H               | 2.53                         | sr,avx512        | Max walltime of 2 days                  |
| long           | 2     | 72         | 710000      | Intel(R) Xeon(R) Gold 6254 CPU @ 3.10GHz  | 1.26                         | physg5,avx512    | Max walltime of 90 days                 |
| bigmem         | 10    | 72         | 2970000     | Intel(R) Xeon(R) Gold 6254 CPU @ 3.10GHz  | 5.07                         | physg5,avx512    | Max walltime of 14 days                 |
| cascade        | 53    | 72         | 710000      | Intel(R) Xeon(R) Gold 6254 CPU @ 3.10GHz  | 46.89 (0.63 per node)        | physg5,avx512    | Max walltime of 30 days                 |
| sapphire       | 48    | 128        | 977000      | Intel(R) Xeon(R) Gold 6448H               | 143.75 (1.84 per node)       | sr,avx512        | Max walltime of 30 days                 |
|                | 26    | 128        | 2001000     | Intel(R) Xeon(R) Gold 6448H               | 143.75 (1.84 per node)       | sr,avx512        | Max walltime of 30 days                 |
| gpu-a100       | 29    | 32         | 495000      | Intel(R) Xeon(R) Gold 6326 CPU @ 2.90GHz  |                              | avx512           | 4 80GB A100 Nvidia GPUs per node. Max walltime of 7 days     |
| gpu-a100-short | 2     | 32         | 495000      | Intel(R) Xeon(R) Gold 6326 CPU @ 2.90GHz  |                              | avx512           | 4 80GB A100 Nvidia GPUs per node. Max walltime of 4 hrs      |
| gpu-h100       | 10    | 64         | 950000      | Intel(R) Xeon(R) Platinum 8462Y+ @ 2.80GHz |                             | avx512           | 4 80GB H100 SXM5 Nvidia GPUs per node. Max walltime of 7 days |
| Total          |       |            |             |                                           | 199.5 (CPU) + 1358 (GPU)     |                  |                                         |

**sapphire and cascade**

Each node is connected by high-speed 50Gb networking with 1.5 µsec latency, making this partition suited to multi-node jobs (e.g. those using OpenMPI).

You can constrain your jobs to use different groups of nodes (e.g. just the Intel(R) Xeon(R) Gold 6154 CPU @ 3.00GHz nodes) by adding `#SBATCH --constraint=physg4` to your submit script

**gpu-a100**

The gpu-a100 partition has modern, 80GB Nvidia A100 GPUs. See [the GPU page](/gpu) for more details.

**gpu-h100**

The gpu-h100 partition has modern, 80GB Nvidia H100 SXM5 GPUs. See [the GPU page](/gpu) for more details.

**AVX-512**

A number of nodes make use of the AVX-512 extended instructions. These include all of the nodes in the cascade partition, and the login nodes. To submit a job on the cascade partition that makes use of these instructions add `#SBATCH --constraint=avx512` in your submission script.

**Private partitions**

Spartan hosts a number of private partitions, where the hardware is owned by individual research groups and faculties.

FEIT research staff and students: feit-gpu-a100  

Computing and Information Systems research staff and students: deeplearn

Faculty of Science research staff and students: fos-gpu-l40s

The partition information for the private partitions can be found on [Specs for private partitions](/privatespecs)

**Other Partitions**

There are also special partitions which are outside normal walltime constraints. In particular, `interactive` should be used for quick test cases; `interactive` has a maximum time of 2 days.

## Storage system

Spartan uses [IBM Spectrum Scale](https://www.ibm.com/support/knowledgecenter/en/SSFKCN/gpfs_welcome.html) (previously known as GPFS). This is a highly scalable, parallel and robust filesystem.

The total Spartan storage is broken up into 2 areas:

| Location     | Capacity | Disk type     |
|--------------|----------|---------------|
| /data/gpfs   | 2.34PB   | 10K SAS       |
| /data/scratch| 575TB    | Flash 	  |

/home is on the University's NetApp NFS platform, backed by SSD


### Wait Time
*How long will my job take to start?*

<div id="divwaittime" style="width: 85%; margin: 0 auto;"></div>

<div class="container">
  <div class="row" style="width:50%; margin: 0 auto;">
    <div class="col-sm">
      Partition: <select id="partitionSelect" class="alt"></select>
    </div>
    <div class="col-sm">
      CPUs Requested: <select id="coreSelect" class="alt"></select>
    </div>
    <div class="col-sm">
      Wall Time: <select id="wallTimeSelect" class="alt"></select>
    </div>
  </div>
</div>


<br/>

<ul class="accordion">
  <li>
<span class="accordion__title">How to Interpret</span>
<div class="accordion__hidden">
<ul>
<li>This plot provides data on how long previous jobs have taken to start, which can be used as guidance on how long your job might take to start.</li>
<li>Note however that "Past performance is no guarantee of future results"; wait times can fluctuate quickly due to changes in usage or outages, and wait time could be considerably more or less than the historic average.</li>
<li>Daily averages are shown, but points may be missing for days where there were no jobs matching the selected characteristics.</li>
<li>This data is acquired using the <code>sacct</code> command in Slurm to get a list of all recent jobs and their start/end times.</li>
</ul>
</div>
</li>
</ul>


<script>

    window.onload = function() {

        Plotly.d3.json("/jsondata/wait_time.json" + '?' + Math.floor(Math.random() * 1000), function (data) {

            function assignOptions(options, selector) {
                $.each(options, function (val, text) {
                    selector.append(
                        $('<option></option>').val(text).html(text)
                    );
                });
            }

            function updatePlot() {
                var graphData = [
                    {   x: data['data'][wallTimeSelect.val()][partitionSelect.val()][coreSelect.val()]['x'],
                        y: data['data'][wallTimeSelect.val()][partitionSelect.val()][coreSelect.val()]['y'],
                        text: 'hours',
                        mode: 'markers',
                        hoverinfo: 'x+text+y',
                        marker: {
                            size: 10
                        },
                        name: 'Daily Average'
                    },
                    {
                        x: data['data'][wallTimeSelect.val()][partitionSelect.val()][coreSelect.val()]['x'],
                        y: data['data'][wallTimeSelect.val()][partitionSelect.val()][coreSelect.val()]['y_rolling_average'],
                        hoverinfo: 'none',
                        line: {
                            color: 'rgb(150, 150, 150)',
                            width: 1,
                            dash: 'dash'
                        },
                        name: 'Rolling 14d Mean'
                    }
                ];

                var layout = {
                    title: 'Job Wait Time',
                    width: 700,
                    height: 400,
                    yaxis: {
                        title: 'Hours',
                        hoverformat: '.2f hours'
                    },
                    legend: {
                        x: 0.5, 
                        y: 1.2,
                        orientation: 'h',
                        xanchor: 'center',
                    }
                };
                Plotly.newPlot('divwaittime', graphData, layout);
            }

            var partitionSelect = $('#partitionSelect');
            var coreSelect = $('#coreSelect');
            var wallTimeSelect = $('#wallTimeSelect');

            // Populate drop-down options
            assignOptions(data['options']['partitions'], partitionSelect);
            assignOptions(data['options']['cores'], coreSelect);
            assignOptions(data['options']['wall_times'], wallTimeSelect);

            // Attach listeners
            partitionSelect.change(updatePlot);
            coreSelect.change(updatePlot);
            wallTimeSelect.change(updatePlot);

            // Build initial plot
            updatePlot()

        });
    }


</script>


## Specifications

Spartan has a number of partitions available for general usage. A full list of partitions can be viewed with the command `sinfo -s`.

| Partition      | Nodes | Cores per node | Memory per node (MB) | Processor                    | Peak Performance (DP TFlops) | Slurm node types |Extra notes                              |
|----------------|-------|------------|-------------|-------------------------------------------|------------------------------|------------------|-----------------------------------------|
| interactive    | 4     | 72         | 710000      | Intel(R) Xeon(R) Gold 6254 CPU @ 3.10GHz  | 2.53                         | physg5,avx512    | Max walltime of 2 days                  |
| long           | 2     | 72         | 710000      | Intel(R) Xeon(R) Gold 6254 CPU @ 3.10GHz  | 1.26                         | physg5,avx512    | Max walltime of 90 days                 |
| bigmem         | 8     | 72         | 2970000     | Intel(R) Xeon(R) Gold 6254 CPU @ 3.10GHz  | 5.07                         | physg5,avx512    | Max walltime of 14 days                 |
| cascade        | 74    | 72         | 710000      | Intel(R) Xeon(R) Gold 6254 CPU @ 3.10GHz  | 46.89 (0.63 per node)        | physg5,avx512    | Max walltime of 30 days                 |
| sapphire       | 52    | 128        | 977000      | Intel(R) Xeon(R) Gold 6448H               | 143.75 (1.84 per node)       | sr,avx512        | Max walltime of 30 days                 |
|                | 26    | 128        | 2001000     | Intel(R) Xeon(R) Gold 6448H               | 143.75 (1.84 per node)       | sr,avx512        | Max walltime of 30 days                 |
| gpu-a100       | 29    | 32         | 495000      | Intel(R) Xeon(R) Gold 6326 CPU @ 2.90GHz  |                              | avx512           | 4 80GB A100 Nvidia GPUs per node. Max walltime of 7 days     |
| gpu-a100-short | 2     | 32         | 495000      | Intel(R) Xeon(R) Gold 6326 CPU @ 2.90GHz  |                              | avx512           | 4 80GB A100 Nvidia GPUs per node. Max walltime of 4 hrs      |
| gpu-h100       | 10    | 64         | 950000      | Intel(R) Xeon(R) Platinum 8462Y+ @ 2.80GHz |                             | avx512           | 4 80GB H100 SXM5 Nvidia GPUs per node. Max walltime of 7 days |
| Total          |       |            |             |                                           | 199.5 (CPU) + 1358 (GPU)     |                  |                                         |

**sapphire and cascade**

Each node is connected by high-speed 50Gb networking with 1.5 µsec latency, making this partition suited to multi-node jobs (e.g. those using OpenMPI).

You can constrain your jobs to use different groups of nodes (e.g. just the Intel(R) Xeon(R) Gold 6154 CPU @ 3.00GHz nodes) by adding `#SBATCH --constraint=physg4` to your submit script

**gpu-a100**

The gpu-a100 partition has modern, 80GB Nvidia A100 GPUs. See [the GPU page](/gpu) for more details.

**gpu-h100**

The gpu-h100 partition has modern, 80GB Nvidia H100 SXM5 GPUs. See [the GPU page](/gpu) for more details.

**AVX-512**

A number of nodes make use of the AVX-512 extended instructions. These include all of the nodes in the cascade partition, and the login nodes. To submit a job on the cascade partition that makes use of these instructions add `#SBATCH --constraint=avx512` in your submission script.

**Private partitions**

Spartan hosts a number of private partitions, where the hardware is owned by individual research groups and faculties.

FEIT research staff and students: feit-gpu-a100  

Computing and Information Systems research staff and students: deeplearn

Faculty of Science research staff and students: fos-gpu-l40s

The partition information for the private partitions can be found on [Specs for private partitions](/privatespecs)

**Other Partitions**

There are also special partitions which are outside normal walltime constraints. In particular, `interactive` should be used for quick test cases; `interactive` has a maximum time of 2 days.

## Storage system

Spartan uses [IBM Spectrum Scale](https://www.ibm.com/support/knowledgecenter/en/SSFKCN/gpfs_welcome.html) (previously known as GPFS). This is a highly scalable, parallel and robust filesystem.

The total Spartan storage is broken up into 2 areas:

| Location     | Capacity | Disk type     |
|--------------|----------|---------------|
| /data/gpfs   | 2.34PB   | 10K SAS       |
| /data/scratch| 575TB    | Flash 	  |

/home is on the University's NetApp NFS platform, backed by SSD

