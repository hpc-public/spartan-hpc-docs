#!/bin/bash
# cd /home/unimelb.edu.au/llafayette/repos/UniMelb/spartan-hpc-docs/
scp spartan:/home/lev/weather/softwarelist.txt docs/
# Removed following 2023 upgrade
# scp spartan:/home/lev/weather/softwarelist_old.txt docs/
scp spartan:/home/lev/weather/status.md .
git pull
sleep 10
sed -i '1,11d' docs/status_specs.md
cat docs/status_specs.md >> status.md && cp status.md docs/status_specs.md
git add docs/status_specs.md
git add docs/softwarelist.txt
git commit -a -m 'Weather report and module list'
git push
